<?php
class Validators {
    private static $instances = [];

    public function __construct() { }

    public function validInteger($value): bool {
        if (!isset($value)) {
            return false;
        }
        return preg_match('/^\d+$/', $value);
    }
    public function validString($value, int $minLen = NULL, int $maxLen = NULL): bool {
        if (!isset($value)) {
            return false;
        }
        $valueLen = strlen($value);
        if (isset($minLen) && $valueLen < $minLen) {
            return false;
        }
        if (isset($maxLen) && $valueLen > $maxLen) {
            return false;
        }
        return true;
    }

    public function validDate($date): bool {
        if (!isset($date)) {
            return false;
        }
        return (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date));
    }

    public function validEmail($email): bool {
        if (!isset($email)) {
            return false;
        }
        return (filter_var($email, FILTER_VALIDATE_EMAIL));
    }

    protected function __clone() { }

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance(): Validators
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static();
        }
        return self::$instances[$cls];
    }
}