<?php
class Database
{
    private static $instances = [];

    private $driver;
    private $host;
    private $user;
    private $pass;
    private $database;
    private $charset;
    private $connection;
    private $connected;

    public function __construct()
    {
        $config = require_once SITE_ROOT . 'config/database.php';
        $this->driver=$config["driver"];
        $this->host=$config["host"];
        $this->user=$config["user"];
        $this->pass=$config["pass"];
        $this->database=$config["database"];
        $this->charset=$config["charset"];
        $this->connected = false;
    }

    public function getConnection() {
        return $this->connection;
    }

    public function connect()
    {
        if ($this->connected) {
            return $this->getConnection();
        }

        if ($this->driver == "mysql" || $this->driver == null) {
            $this->connection = new mysqli($this->host, $this->user, $this->pass, $this->database);
            if ($this->connection->connect_errno) {
                die("Error de conexión: " . $this->connection->connect_error);
            }
            $this->connected = true;
            $this->connection->query("SET NAMES '".$this->charset."'");
        }

        return $this->getConnection();
    }

    public function executeStatement(string $sql, string $types = NULL, array $params = NULL) : ?array
    {
        $statement = $this->connection->prepare($sql);
        if ($types !== NULL && $params !== NULL)
        {
            $statement->bind_param($types, ...$params);
        }
        $statement->execute();
        $result = $statement->get_result();
        $statement->close();
        if ($result != false) {
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return NULL;
        }
    }

    public function close()
    {
        if ($this->connected) {
            $this->connection->close();
            $this->connected = false;
        }
    }

    protected function __clone() { }

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance(): Database
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static();
        }
        return self::$instances[$cls];
    }
}
