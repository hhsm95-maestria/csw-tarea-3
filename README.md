# Tarea 3. Manejo de datos en el servidor e interacción con el cliente mediante una aplicación web

## Información general

* **Fecha:** 10/07/2021
* **Alumno:** Héctor Hugo Sandoval Marcelo
* **Asignatura:** Computación en el servidor Web
* **Repositorio:** [https://gitlab.com/hhsm95-maestria/csw-tarea-3](https://gitlab.com/hhsm95-maestria/csw-tarea-3)
* **Ver en línea:** [https://maestria-hhsm95.000webhostapp.com/semestre-1/computacion-servidor-web/tarea-3/](https://maestria-hhsm95.000webhostapp.com/semestre-1/computacion-servidor-web/tarea-3)

## Objetivo

En este laboratorio se pretende diseñar y desarrollar una aplicación web. La finalidad es practicar los conocimientos adquiridos en la asignatura hasta el momento, principalmente los relacionados con el acceso a bases de datos y la interacción con el cliente, así como el diseño mediante la arquitectura de aplicaciones web usando el patrón Modelo Vista Controlador (MVC). Para ello, se puede partir del desarrollo web realizado en el anterior Trabajo, o bien partir de un nuevo/diferente desarrollo web.

Los objetivos requeridos en el laboratorio son los siguientes:

* Diseño de la solución haciendo uso del patrón MVC (en caso de dificultad en su uso, se puede utilizar otro diseño y también se valorará, aunque para alcanzar la máxima puntuación será requerido el uso de MVC).
* Inclusión de formularios y tratamiento de datos en el lado del servidor.
* Conexión, consulta, inserción, actualización y borrado de datos. Se valorará positivamente que se usen otros atributos de my_sqli.
* Uso de clases, valorando positivamente la herencia.

## Resultados


Se implementaron dos vistas, dos modelos y dos controladores:

* Pacientes
* Estudios

### Instrucciones para probar en local

1. Clonar este repositorio dentro del directorio del servidor Apache (ejemplo: `C:/xampp/htdocs/tarea-3/`).
2. Ejecutar la query SQL que se muestra mas abajo para crear la base de datos.
3. Editar el archivo `config/database.php` actualizando los datos para conectarse a su base de datos.
4. Editar el archiv `utils/constants.php` cambiando el valor de `SITE_URL` por el de la ruta de este proyecto dentro de htdocs (ejemplo: `/tarea-3/`)

Tip. El proyecto puede probarse en línea en esta dirección: [https://maestria-hhsm95.000webhostapp.com/semestre-1/computacion-servidor-web/tarea-3/](https://maestria-hhsm95.000webhostapp.com/semestre-1/computacion-servidor-web/tarea-3)

### Modelos

El modelo de Pacientes cuenta con los campos:

![Modelo de paciente](./docs/patient_model.PNG "Modelo de paciente")

El modelo de Estudios cuenta con los campos:

![Modelo de estudio](./docs/study_model.PNG "Modelo de estudio")

### Script SQL

Se adjuntan los scripts SQL para preparar la base de datos:

```sql
CREATE DATABASE TAREA_3;
USE TAREA_3;

CREATE TABLE TAREA_3.PATIENT (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    birth_date DATE NOT NULL,
    email VARCHAR(50) NOT NULL,

    PRIMARY KEY (id)
);


CREATE TABLE TAREA_3.STUDY (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    urgency_level VARCHAR(50) NOT NULL,
    description VARCHAR(50),
    patient_id INT UNSIGNED NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (patient_id) REFERENCES TAREA_3.PATIENT(id)
);


INSERT INTO TAREA_3.PATIENT (first_name, last_name, birth_date, email)
VALUES ('Hugo', 'Sandoval', '1995-12-13', 'hhsm95@outlook.com');

SET @patient_id_1 = LAST_INSERT_ID();

INSERT INTO TAREA_3.PATIENT (first_name, last_name, birth_date, email)
VALUES ('Juan', 'Ramírez', '1993-05-25', 'jramirez@gmail.dev');

SET @patient_id_2 = LAST_INSERT_ID();

INSERT INTO TAREA_3.STUDY (urgency_level, description, patient_id)
VALUES ('Alto', 'Accidente de tráfico', @patient_id_1),
       ('Medio', 'Caida de escaleras', @patient_id_2);

```

### Vistas

Captura de la pantalla de Paciente:
![Vista de paciente](./docs/patient_view.PNG "Vista de paciente")

Captura de la pantalla de Estudios:
![Vista de estudio](./docs/study_view.PNG "Vista de estudio")
