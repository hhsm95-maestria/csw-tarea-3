<?php

require_once SITE_ROOT . 'models/base.php';
require_once SITE_ROOT . 'utils/constants.php';

class PatientModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct("PATIENT");
    }

    private function validation(array $payload): array
    {
        if (!$this->validators->validString($payload["first_name"] ?? NULL, MIN_CHAR_LENGTH, MAX_CHAR_LENGTH)) {
            return array("success"=>false, "message"=>"Nombre(s) no válido(s)");
        }

        if (!$this->validators->validString($payload["last_name"] ?? NULL, MIN_CHAR_LENGTH, MAX_CHAR_LENGTH)) {
            return array("success"=>false, "message"=>"Apellido(s) no válido(s)");
        }

        if (!$this->validators->validDate($payload["birth_date"] ?? NULL)) {
            return array("success"=>false, "message"=>"Fecha de nacimiento no válida");
        }

        if (!$this->validators->validEmail($payload["email"] ?? NULL)) {
            return array("success"=>false, "message"=>"Correo no válido");
        }
        return array("success"=>true);
    }

    public function create(array $payload): array
    {
        $result = $this->validation($payload);
        if (!$result["success"]) {
            return $result;
        }

        $sql = "INSERT INTO {$this->table} (first_name, last_name, birth_date, email) VALUES (?, ?, ?, ?);";
        return parent::_create($sql, $payload, 'ssss');
    }

    public function update(int $id, array $payload): array
    {
        $result = $this->validation($payload);
        if (!$result["success"]) {
            return $result;
        }

        $sql = "UPDATE {$this->table} SET first_name = ?, last_name = ?, birth_date = ?, email = ? WHERE id = ?;";
        return parent::_update($sql, $id, 'ssssi', $payload);
    }
}
