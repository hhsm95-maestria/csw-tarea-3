<?php

require_once SITE_ROOT . 'models/base.php';
require_once SITE_ROOT . 'utils/constants.php';

class StudyModel extends BaseModel
{
    private $patient;

    public function __construct()
    {
        parent::__construct("STUDY");
        require_once SITE_ROOT . 'models/patient.php';
        $this->patient = new PatientModel();
    }

    private function validation(array $payload): array
    {
        $urgency_levels = array("Bajo", "Medio", "Alto");
        if (!$this->validators->validString($payload["urgency_level"], MIN_CHAR_LENGTH, MAX_CHAR_LENGTH) || !in_array($payload["urgency_level"], $urgency_levels)) {
            return array("success"=>false, "message"=>"Nivel de urgencia no válido");
        }

        if (!$this->validators->validString($payload["description"] ?? NULL, MIN_CHAR_LENGTH, MAX_CHAR_LENGTH)) {
            return array("success"=>false, "message"=>"Descripción no válida");
        }

        if (!$this->validators->validInteger($payload["patient_id"]) || $payload["patient_id"] <= 0) {
            return array("success"=>false, "message"=>"ID no válido: {$payload['patient_id']}");
        }

        $row = $this->patient->detail($payload["patient_id"]);
        if (!$row["success"]) {
            return $row;
        }

        return array("success"=>true);
    }

    public function retrieve(): array
    {
        $rows = parent::retrieve();
        $num_rows = count($rows);
        for ($i=0; $i < $num_rows; $i++) {
            $resp = $this->patient->detail($rows[$i]["patient_id"]);
            if ($resp["success"]) {
                $rows[$i]["patient"] = $resp["data"];
            }
        }
        return $rows;
    }

    public function create(array $payload): array
    {
        $result = $this->validation($payload);
        if (!$result["success"]) {
            return $result;
        }

        $sql = "INSERT INTO {$this->table} (urgency_level, description, patient_id) VALUES (?, ?, ?);";
        return parent::_create($sql, $payload, 'ssi');
    }

    public function update(int $id, array $payload): array
    {
        $result = $this->validation($payload);
        if (!$result["success"]) {
            return $result;
        }

        $sql = "UPDATE {$this->table} SET urgency_level = ?, description = ?, patient_id = ? WHERE id = ?;";
        return parent::_update($sql, $id, 'ssii', $payload);
    }
}
