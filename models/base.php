<?php

class BaseModel
{
    protected $db;
    protected $validators;
    protected $table;

    public function __construct(string $table)
    {
        require_once SITE_ROOT . 'utils/database.php';
        require_once SITE_ROOT . 'utils/validators.php';
        $this->db = Database::getInstance();
        $this->validators = Validators::getInstance();
        $this->table = $table;
    }

    public function retrieve(): array
    {
        $sql = "SELECT * FROM {$this->table};";
        $this->db->connect();
        $data = $this->db->executeStatement($sql);
        $this->db->close();
        return $data;
    }

    public function detail(int $id): array
    {
        if (!$this->validators->validInteger($id) || $id <= 0) {
            return array("success"=>false, "message"=>"ID no válido: {$id}");
        }

        $sql = "SELECT * FROM {$this->table} WHERE id = ?;";
        $this->db->connect();
        $data = $this->db->executeStatement($sql, 'i', [$id]);
        $this->db->close();
        if (count($data) == 0) {
            return array("success"=>false, "message"=>"ID no econtrado: {$id}");
        }
        return array("success"=>true, "data"=>$data[0]);
    }

    protected function _create(string $sql, array $payload, string $types): array
    {
        $values = array_values($payload);
        $this->db->connect();
        $this->db->executeStatement($sql, $types, $values);
        $id = $this->db->getConnection()->insert_id;
        $this->db->close();
        if (!isset($id) || $id == 0) {
            return array("success"=>false, "message"=>"Error al crear el registro");
        }
        return array("success"=>true, "data"=>array("id" => $id));
    }

    protected function _update(string $sql, int $id, string $types, array $payload): array
    {
        if (!$this->validators->validInteger($id) || $id < 1) {
            return array("success"=>false, "message"=>"ID no válido: {$id}");
        }
        $row = $this->detail($id);
        if (!$row["success"]) {
            return array("success"=>false, "message"=>"ID no existe: {$id}");
        }

        $values = array_values($payload);
        $this->db->connect();
        $values[] = $id;
        $this->db->executeStatement($sql, $types, $values);
        $this->db->close();
        return array("success"=>true, "data"=>array("id" => $id));
    }

    public function delete(int $id): array
    {
        if (!$this->validators->validInteger($id) || $id <= 0) {
            return array("success"=>false, "message"=>"ID no válido: {$id}");
        }
        $row = $this->detail($id);
        if (!$row["success"]) {
            return array("success"=>false, "message"=>"ID no existe: {$id}");
        }

        $sql = "DELETE FROM {$this->table} WHERE id = ?;";
        $this->db->connect();
        $this->db->executeStatement($sql, 'i', [$id]);

        $row = $this->detail($id);
        if ($row["success"]) {
            return array("success"=>false, "message"=>"No se pudo eliminar el ID: {$id}. Posible dependencia de llave foranea.");
        }

        return array("success"=>true, "data"=>array("id" => $id));
    }
}
