<?php

require_once '../utils/constants.php';

require_once SITE_ROOT . 'controllers/patient.php';

$controller = new PatientController();

$id = isset($_GET['id']) ? $_GET['id'] : null;
$method = isset($_GET['method']) ? $_GET['method'] : null;

$patient = null;
$result = null;

$showAlert = false;
$alertMessage = null;
$alertColor = null;

if (isset($id)) {
    switch ($method) {
      case "GET":
          $patient = $controller->getById($id);
          break;
      case "PUT":
          $result = $controller->updateById($id, $_POST);
          $showAlert = true;
          if ($result["success"]) {
              $alertMessage = "Se actualizó el registro con el ID: {$result['data']['id']}";
              $alertColor = "success";
          } else {
              $alertMessage = "Error: {$result['message']}";
              $alertColor = "danger";
          }
          break;
      case "DELETE":
          $result = $controller->deleteById($id);
          $showAlert = true;
          if ($result["success"]) {
              $alertMessage = "Se eliminó el registro con el ID: {$result['data']['id']}";
              $alertColor = "success";
          } else {
              $alertMessage = "Error: {$result['message']}";
              $alertColor = "danger";
          }
          break;
    }
} else {
  if ($method == "POST") {
    $result = $controller->create($_POST);
    $showAlert = true;
    if ($result["success"]) {
        $alertMessage = "Se creó el registro con el ID: {$result['data']['id']}";
        $alertColor = "success";
    } else {
        $alertMessage = "Error: {$result['message']}";
        $alertColor = "danger";
    }
  }
}
$patients = $controller->getAll();

?>

<!doctype html>
<html lang="es-MX">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">


    <style>
        body {
            color: var(--bs-light);
        }

        .u-bg-dark {
            background-color: var(--bs-gray-dark);
        }

        .u-bg-darker {
            background-color: var(--bs-dark);
        }

        #map {
            height: 400px;
        }
    </style>
    <title>Vista Pacientes</title>
</head>

<body class="u-bg-dark">

<main class="container">
    <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Tarea 3</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo(SITE_URL . "views/patient.php"); ?>">Pacientes <span class="sr-only">(actual)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo(SITE_URL . "views/study.php"); ?>">Estudios</a>
            </li>
            </ul>
        </div>
    </nav>

    <div class="row mt-2 mb-4">
      <div clas="col">
        <h3 class="text-center u-bg-darker rounded p-2">Vista Pacientes</h3>
      </div>
    </div>

    <?php if ($showAlert) { ?>
      <div class="alert alert-<?php echo($alertColor); ?>" role="alert">
        <?php echo($alertMessage); ?>
      </div>
    <?php } ?>

    <div class="row mt-2 mb-4">
      <div class="col-8">
        <table class="table table-dark table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre(s)</th>
                    <th scope="col">Apellido(s)</th>
                    <th scope="col">Fecha de nacimiento</th>
                    <th scope="col">Email</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($patients as $row) { ?>
                <tr>
                    <td><?php echo($row["id"]); ?></td>
                    <td><?php echo($row["first_name"]); ?></td>
                    <td><?php echo($row["last_name"]); ?></td>
                    <td><?php echo($row["birth_date"]); ?></td>
                    <td><?php echo($row["email"]); ?></td>
                    <td>
                    <div class="btn-group" role="group">
                        <a class="btn btn-warning btn-sm" href="<?php echo($controller->getGetUrl($row["id"])); ?>"><i class="bi bi-pencil"></i></a>
                        <a class="btn btn-danger btn-sm" href="<?php echo($controller->getDeleteUrl($row["id"])); ?>"><i class="bi bi-person-x"></i></a>
                    </div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
      </div>
      <div class="col-4">
        <?php
        $form_action = $method != "GET" ? $controller->getPostUrl() : null;
        $form_first_name = "";
        $form_last_name = "";
        $form_birth_date = "";
        $form_email = "";
        if (isset($patient) && $patient["success"]) {
            $form_id = $patient["data"]["id"];
            $form_action = $controller->getPutUrl($form_id);

            $form_first_name = $patient["data"]["first_name"];
            $form_last_name = $patient["data"]["last_name"];
            $form_birth_date =$patient["data"]["birth_date"];
            $form_email = $patient["data"]["email"];
        }
        ?>
        <form class="u-bg-darker rounded p-2" method="post" action="<?php echo($form_action); ?>" >
          <div class="mb-3">
            <label for="txtFirstName" class="form-label">Nombre(s):</label>
            <input type="text" class="form-control" id="txtFirstName" name="first_name" placeholder="Juan Manuel" value="<?php echo($form_first_name); ?>">
          </div>
          <div class="mb-3">
            <label for="txtLastName" class="form-label">Apellido(s):</label>
            <input type="text" class="form-control" id="txtLastName" name="last_name" placeholder="Pérez Martínez" value="<?php echo($form_last_name); ?>">
          </div>
          <div class="mb-3">
            <label for="txtBirthDate" class="form-label">Fecha de nacimiento:</label>
            <input type="date" class="form-control" id="txtBirthDate" name="birth_date" placeholder="" value="<?php echo($form_birth_date); ?>">
          </div>
          <div class="mb-3">
            <label for="txtEmail" class="form-label">Email:</label>
            <input type="email" class="form-control" id="txtEmail" name="email" placeholder="jperezm@gmail.com" value="<?php echo($form_email); ?>">
          </div>
          <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
      </div>
    </div>

</main>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>

</body>

</html>