<?php

require_once '../utils/constants.php';

require_once SITE_ROOT . 'controllers/study.php';
require_once SITE_ROOT . 'controllers/patient.php';

$controller = new StudyController();
$patientController = new PatientController();

$patients = $patientController->getAll();

$id = isset($_GET['id']) ? $_GET['id'] : null;
$method = isset($_GET['method']) ? $_GET['method'] : null;

$patient = null;
$result = null;

$showAlert = false;
$alertMessage = null;
$alertColor = null;

if (isset($id)) {
    switch ($method) {
      case "GET":
          $study = $controller->getById($id);
          break;
      case "PUT":
          $result = $controller->updateById($id, $_POST);
          $showAlert = true;
          if ($result["success"]) {
              $alertMessage = "Se actualizó el registro con el ID: {$result['data']['id']}";
              $alertColor = "success";
          } else {
              $alertMessage = "Error: {$result['message']}";
              $alertColor = "danger";
          }
          break;
      case "DELETE":
          $result = $controller->deleteById($id);
          $showAlert = true;
          if ($result["success"]) {
              $alertMessage = "Se eliminó el registro con el ID: {$result['data']['id']}";
              $alertColor = "success";
          } else {
              $alertMessage = "Error: {$result['message']}";
              $alertColor = "danger";
          }
          break;
    }
} else {
    if ($method == "POST") {
        $result = $controller->create($_POST);
        $showAlert = true;
        if ($result["success"]) {
            $alertMessage = "Se creó el registro con el ID: {$result['data']['id']}";
            $alertColor = "success";
        } else {
            $alertMessage = "Error: {$result['message']}";
            $alertColor = "danger";
        }
    }
}
$studies = $controller->getAll();

?>

<!doctype html>
<html lang="es-MX">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">


    <style>
        body {
            color: var(--bs-light);
        }

        .u-bg-dark {
            background-color: var(--bs-gray-dark);
        }

        .u-bg-darker {
            background-color: var(--bs-dark);
        }

        #map {
            height: 400px;
        }
    </style>
    <title>Vista Estudios</title>
</head>

<body class="u-bg-dark">

<main class="container">
<nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Tarea 3</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo(SITE_URL . "views/patient.php"); ?>">Pacientes</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo(SITE_URL . "views/study.php"); ?>">Estudios <span class="sr-only">(actual)</a>
            </li>
            </ul>
        </div>
    </nav>

    <div class="row mt-2 mb-4">
      <div clas="col">
        <h3 class="text-center u-bg-darker rounded p-2">Vista Estudios</h3>
      </div>
    </div>

    <?php if ($showAlert) { ?>
      <div class="alert alert-<?php echo($alertColor); ?>" role="alert">
        <?php echo($alertMessage); ?>
      </div>
    <?php } ?>

    <div class="row mt-2 mb-4">
      <div class="col-8">
        <table class="table table-dark table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nivel de Urgencia</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Paciente</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($studies as $row) { ?>
                <tr>
                    <td><?php echo($row["id"]); ?></td>
                    <td><?php echo($row["urgency_level"]); ?></td>
                    <td><?php echo($row["description"]); ?></td>
                    <td><?php echo($row["patient"]["first_name"] . " " . $row["patient"]["last_name"]); ?></td>
                    <td>
                    <div class="btn-group" role="group">
                        <a class="btn btn-warning btn-sm" href="<?php echo($controller->getGetUrl($row["id"])); ?>"><i class="bi bi-pencil"></i></a>
                        <a class="btn btn-danger btn-sm" href="<?php echo($controller->getDeleteUrl($row["id"])); ?>"><i class="bi bi-person-x"></i></a>
                    </div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
      </div>
      <div class="col-4">
        <?php
        $form_action = $method != "GET" ? $controller->getPostUrl() : null;
        $form_urgency_level = "";
        $form_description = "";
        $form_patient_id = "";
        if (isset($study) && $study["success"]) {
            $form_id = $study["data"]["id"];
            $form_action = $controller->getPutUrl($form_id);

            $form_urgency_level = $study["data"]["urgency_level"];
            $form_description = $study["data"]["description"];
            $form_patient_id =$study["data"]["patient_id"];
        }
        ?>
        <form class="u-bg-darker rounded p-2" method="post" action="<?php echo($form_action); ?>" >
          <div class="mb-3">
            <select class="form-select" name="urgency_level">
                <option <?php echo($method != "GET" ? "selected" : "");?>>Nivel de urgencia:</option>
                <option value="Bajo" <?php echo($form_urgency_level == "Bajo" ? "selected" : "");?>>Bajo</option>
                <option value="Medio" <?php echo($form_urgency_level == "Medio" ? "selected" : "");?>>Medio</option>
                <option value="Alto" <?php echo($form_urgency_level == "Alto" ? "selected" : "");?>>Alto</option>
            </select>
          </div>
          <div class="mb-3">
            <label for="txtDescription" class="form-label">Descripción:</label>
            <textarea class="form-control" id="txtDescription" name="description" placeholder="Ejem: Dolor de cabeza"><?php echo($form_description); ?></textarea>
          </div>
          <div class="mb-3">
            <select class="form-select" name="patient_id">
                <option <?php echo($method != "GET" ? "selected" : "");?>>Paciente:</option>
                <?php foreach ($patients as $row) { ?>
                <option value="<?php echo($row["id"]); ?>" <?php echo($row["id"] == $form_patient_id ? "selected" : "") ?>><?php echo($row["first_name"] . " " . $row["last_name"]); ?></option>
                <?php } ?>
            </select>
          </div>
          <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
      </div>
    </div>

</main>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>

</body>

</html>