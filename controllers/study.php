<?php

require_once SITE_ROOT . 'controllers/base.php';

class StudyController extends BaseController
{
    public function __construct()
    {
        require_once SITE_ROOT . 'models/study.php';
        $studyModel = new StudyModel();
        parent::__construct($studyModel, "study");
    }
}