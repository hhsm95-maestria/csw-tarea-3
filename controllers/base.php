<?php

require_once SITE_ROOT . 'models/base.php';

class BaseController
{
    protected $model;
    protected $view;

    public function __construct($model, string $view)
    {
        $this->model = $model;
        $this->view = $view;
    }

    public function getGetUrl(int $id): string {
        return SITE_URL . "views/{$this->view}.php?id={$id}&method=GET";
    }

    public function getPostUrl(): string {
        return SITE_URL . "views/{$this->view}.php?method=POST";
    }

    public function getPutUrl(int $id): string {
        return SITE_URL . "views/{$this->view}.php?id={$id}&method=PUT";
    }

    public function getDeleteUrl(int $id): string {
        return SITE_URL . "views/{$this->view}.php?id={$id}&method=DELETE";
    }

    public function getAll(): array {
        return $this->model->retrieve();
    }

    public function getById(int $id): array {
        return $this->model->detail($id);
    }

    public function create(array $payload): array {
        return $this->model->create($payload);
    }

    public function updateById(int $id, array $payload): array {
        return $this->model->update($id, $payload);
    }

    public function deleteById(int $id): array {
        return $this->model->delete($id);
    }
}