<?php

require_once SITE_ROOT . 'controllers/base.php';

class PatientController extends BaseController
{
    public function __construct()
    {
        require_once SITE_ROOT . 'models/patient.php';
        $patientModel = new PatientModel();
        parent::__construct($patientModel, "patient");
    }

}